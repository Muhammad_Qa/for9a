<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('/auth')->group(function () {

    //
    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'UserController@register');
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/me', 'AuthController@me');
        Route::post('/logout', 'AuthController@logout');
        Route::put('/user/{id}', 'UserController@update');
        // Routes for sending emails
        Route::get('/send-email', 'EmailController@sendEmail');
    });

    Route::post('/reset-password', 'EmailController@resetPassword');
    Route::post("/reset-password/confirm", "EmailController@confirmResetPassword");
    // send email test
    /*Route::get("/send-message", function () {

        try {
            $random_string = Str::random(5);

            Mail::to("elgoharya837@gmail.com")->send(new \App\Mail\templates\ResetPasswordMail($random_string));

            return response()->json(['message' => 'mail sent successful']);

        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    });*/
});